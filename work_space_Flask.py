from euler_function import *
from flask import Flask

app = Flask(__name__)


@app.route('/<int:number>/')
def query(number):
    if number == 1:
        return euler_1()
    elif number == 2:
        return euler_2()
    elif number == 5:
        return euler_5()
    elif number == 6:
        return euler_6()
    elif number == 9:
        return euler_9()
    elif number == 12:
        return euler_12()
    elif number == 20:
        return euler_20()


if __name__ == '__main__':
    app.run()
