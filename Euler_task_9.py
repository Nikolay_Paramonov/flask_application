import time
"""""
Задача Эйлера 9
Тройка Пифагора - три натуральных числа a < b < c, для которых выполняется равенство
a2 + b2 = c2
Например, 32 + 42 = 9 + 16 = 25 = 52.
Существует только одна тройка Пифагора, для которой a + b + c = 1000.
Найдите произведение abc.
"""""


def euler_9():
    start = time.time()
    first = 0
    second = 0
    for a in range(1, 1000):
        for b in range(1, 1000):
            if a + b + (a ** 2 + b ** 2) ** 0.5 == 1000:
                first = a
                second = b
    c = 1000 - first - second
    end = time.time()
    answer_1 = 'Произведение abc' + ' ' + str(first * second * c)
    answer_2 = 'Время выполнения программы' + ' ' + str(end - start)
    return 'Задача Эйлера 9' + '\n' + answer_1 + '\n' + answer_2
