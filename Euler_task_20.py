import time
"""""
Задача Эйлера 20
n! означает n × (n − 1) × ... × 3 × 2 × 1
Например, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
и сумма цифр в числе 10! равна 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
Найдите сумму цифр в числе 100!.
"""""


def euler_20():
    from functools import reduce
    start = time.time()
    a = range(100, 0, -1)
    b = reduce(lambda x, y: x * y, a)
    c = b
    end = time.time()
    answer_1 = 'Сумма цифр в числе 100!' + ' ' + str(sum(map(int, str(c))))
    answer_2 = 'Время выполнения программы' + ' ' + str(end - start)
    return 'Задача Эйлера 20' + '\n' + answer_1 + '\n' + answer_2
